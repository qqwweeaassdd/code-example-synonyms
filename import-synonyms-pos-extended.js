'use strict';

const lineByLine = require('n-readlines');
const XXHash = require('xxhash');
const Buffer = require('buffer').Buffer;

const utils = require('./libs/utils');
const Stemmer = require('./libs/stemmer');
const db = require('./models/index')(null, 'synonyms-pos-extended');
const props = require('./libs/props');
const getFrequencyStems = require('./libs/get-freq');

let liner = new lineByLine('./storage/base/base.txt');

let linesTotal = 0;
let linesWords = 0;
let linesFiltered = 0;
let frequencyFiltered = 0;
let filteredByProps = 0;
let filteredByStems = 0;

let stems = {}; // стемы нашего словаря
let freqStems = {}; // стемы из словаря частотности (наиболее употребимые слова)
let synonyms = {}; // база синонимов

let variantsByHash = {}; // варианты слов по хешам
let variantsByStem = {}; // варианты слов по стемам

/**
 * Получает части строки: стему и окончание слова и характеристики
 * @param line
 */
function getPartsByLine(line) {

    let lineText = line.toString('utf8').toLowerCase();
    if (lineText) {

        let checkRes = /^(.+?)\s+([a-z0-9\-, ]+)/gi.exec(lineText);
        if (checkRes && typeof checkRes[1] !== 'undefined') {

            linesWords++;

            let thisProps = {};
            let word = checkRes[1].replace(/ё/g, 'е');
            let stem = Stemmer.stem(word);

            // фильт по стеме
            if (!stem || word.indexOf(stem) < 0) {
                linesFiltered++;
                console.log(word, stem);
                return;
            }

            // фильтр по частоте
            if (typeof freqStems[stem] === 'undefined') {
                frequencyFiltered++;
                return;
            }

            // не надо с одной буквой перед минусом
            if (word.length === 1 || /^[^-]-/.exec(word) || /-[^-]$/.exec(word) || /-[^-]-/.exec(word)) {
                filteredByStems++;
                return;
            }

            // определяем свойства
            if (typeof checkRes[2] !== 'undefined') {
                checkRes[2].trim()
                    .replace(/\s+/g, ',')
                    .split(',')
                    .filter(item => item)
                    .forEach(prop => {
                        thisProps[prop] = true;
                    });
            }

            // фильтр по свойствам
            if (thisProps['erro'] || thisProps['dist'] || thisProps['init'] || thisProps['abbr'] || thisProps['arch']) {
                filteredByProps++;
                return;
            }

            // пропускаемые части речи
            if (thisProps['intj'] || thisProps['prcl'] || thisProps['conj'] || thisProps['prep'] || thisProps['pred'] || thisProps['npro'] || thisProps['advb']) {
                filteredByProps++;
                return;
            }

            // postfix
            let postfix = '';
            if (stem.length < word.length) {
                postfix = word.slice(stem.length);
            }

            let saveProps = {};
            Object.keys(thisProps)
                .filter(prop => props.using.indexOf(prop) > -1)
                .forEach(prop => {
                    saveProps[prop] = true;
                });

            if (typeof stems[stem] === 'undefined') {
                stems[stem] = [];
            }

            // определим часть речи
            let posIdx;
            for (let idx = 0; idx < props.pos.length; idx++) {
                if (saveProps[props.pos[idx]]) {
                    posIdx = idx;
                    break;
                }
            }

            stems[stem].push({
                postfix: postfix,
                props:   saveProps,
                posIdx:  posIdx
            });
        }
    }
}

/**
 * Получаем футпринт варианта
 * @param getPropId function ункция получения id свойства
 * @param variant object
 * @returns {string}
 */
function buildFootprint(getPropId, variant) {

    let obj = {
        posId:    getPropId(props.pos),
        animId:   (variant.props['numr'] ? null : getPropId(props.anim)),
        genderId: getPropId(props.gender),
        countId:  getPropId(props.count),
        caseId:   (variant.props['adjs'] ? null : getPropId(props.case)),
        perfId:   getPropId(props.perf),
        transId:  getPropId(props.trans),
        persId:   getPropId(props.pers),
        timeId:   getPropId(props.time),
        modId:    getPropId(props.mod),
        voiceId:  getPropId(props.voice),
        name:     (typeof variant.props['name'] !== 'undefined'),
        surn:     (typeof variant.props['surn'] !== 'undefined'),
        patr:     (typeof variant.props['patr'] !== 'undefined'),
        geox:     (typeof variant.props['geox'] !== 'undefined'),
        orgn:     (typeof variant.props['orgn'] !== 'undefined'),
        supr:     (typeof variant.props['supr'] !== 'undefined'),
        qual:     (typeof variant.props['qual'] !== 'undefined'),
        anum:     (typeof variant.props['anum'] !== 'undefined'),
        poss:     (typeof variant.props['poss'] !== 'undefined'),
        impe:     (typeof variant.props['impe'] !== 'undefined'),
        fimp:     (typeof variant.props['fimp'] !== 'undefined')
    };

    if (variant.props['comp'] || variant.props['grnd'] || variant.props['advb'] || variant.props['npro']) {
        obj.postfix = variant.postfix;
    }

    return Object.keys(obj).map(field => +obj[field]).join('-');
}

/**
 * Возвращает функцию ниже
 * @param thisProps object свойства объекта
 * @returns {null|string}
 */
function getPropIdFunc(thisProps) {

    /**
     * Возвращает id из thisProps, если оно имеется в группе для поиска typeProps
     * @param typeProps array список названий свойств
     */
    return typeProps => {
        for (let idx = 0; idx < typeProps.length; idx++) {
            if (typeof thisProps[typeProps[idx]] !== 'undefined') {
                return props.propsRels[typeProps[idx]];
            }
        }

        return null;
    }
}

/**
 * Строит массив вариантов
 * @returns {Array}
 */
function buildVariantsArr() {
    let getPropId;
    let word;
    let hash;
    let variantsArr = {};

    let linesProcessed = 0;
    let stemKeys = Object.keys(stems);

    stemKeys.forEach(stem => {
        linesProcessed++;
        if (linesProcessed % 10000 === 0) {
            console.log(`Processed items ${linesProcessed} / ${stemKeys.length}`);
        }

        if (typeof variantsByStem[stem] === 'undefined') {
            variantsByStem[stem] = [];
        }

        stems[stem].forEach(variant => {

            getPropId = getPropIdFunc(variant.props);

            // часть речи должна быть обязательно
            if (getPropId(props.pos) === null) {
                return;
            }

            word = stem + variant.postfix;
            hash = XXHash.hash(Buffer.from(buildFootprint(getPropId, variant)), 0xCAFEBABE);

            if (typeof variantsArr[word] === 'undefined') {
                variantsArr[word] = [];
            }

            if (typeof variantsByHash[hash] === 'undefined') {
                variantsByHash[hash] = {};
            }

            variantsArr[word].push(hash);
            variantsByHash[hash][word] = true;
            variantsByStem[stem].push(word);
        });
    });
    stems = null;


    // сортируем хеши вариантов
    Object.keys(variantsArr).forEach(word => {
        variantsArr[word].sort();
    });


    return variantsArr;
}

/**
 * Получаем синонимы для сохранения
 * @param variantsArr object
 */
function getSynonyms(variantsArr) {
    // разбор файла
    let liner = new lineByLine('./storage/base/synonyms.txt');
    let line;
    let synLine;

    // разбор синонима
    let wordMain;
    let wordSynonyms;

    // стемы
    let stem;
    let stemMain;
    let stemSynonyms = {};

    let synonymFiltered = 0;
    let linesProcessed = 0;


    // 1. Читаем синонимы и строим стем-скелет
    while (line = liner.next()) {
        linesProcessed++;
        if (linesProcessed % 100000 === 0) {
            console.log(`Processed lines ${linesProcessed/1000}K`);
        }

        try {
            synLine = line.toString('utf8').trim().toLowerCase();
            [wordMain, wordSynonyms] = synLine.split(':');

            // проверяем, что синонимы существуют в списке слов
            if (typeof wordMain === 'undefined' || typeof wordSynonyms === 'undefined') {
                synonymFiltered++;
                continue;
            }


            stemMain = Stemmer.stem(wordMain);
            if (typeof stemSynonyms[stemMain] === 'undefined') {
                stemSynonyms[stemMain] = {};
            }

            wordSynonyms = wordSynonyms.split(',').forEach(word => {
                stem = Stemmer.stem(word);
                stemSynonyms[stemMain][stem] = true;
            });
        } catch(err) { }
    }

    // 2. Выискиваем все варианты по стемам и их вариантам
    console.log('\nSearch variants');
    let idx;
    let jdx;
    let wordSynonym;
    let stemSynonymKeys = Object.keys(stemSynonyms);
    linesProcessed = 0;

    stemSynonymKeys.forEach(stemMain => {
        linesProcessed++;
        if (linesProcessed % 2000 === 0) {
            console.log(`Processed stems ${linesProcessed} / ${stemSynonymKeys.length}`);
        }
        if (typeof variantsByStem[stemMain] === 'undefined') {
            return;
        }

        for (idx = 0; idx < variantsByStem[stemMain].length; idx++) {
            wordMain = variantsByStem[stemMain][idx];
            if (variantsArr[wordMain].length === 0) {
                continue;
            }

            synonyms[wordMain] = {};
            Object.keys(stemSynonyms[stemMain]).forEach(stemSynonym => {
                if (typeof variantsByStem[stemSynonym] === 'undefined') {
                    return;
                }

                for (jdx = 0; jdx < variantsByStem[stemSynonym].length; jdx++) {
                    wordSynonym = variantsByStem[stemSynonym][jdx];

                    if (utils.superBag(variantsArr[wordMain], variantsArr[wordSynonym])) {
                        synonyms[wordMain][wordSynonym] = true;
                    }
                }
            });

            // удалить "самого себя" из синонимов
            delete synonyms[wordMain][wordMain];

            synonyms[wordMain] = Object.keys(synonyms[wordMain]);
        }
    });

    // 3.0. Убираем дурацкие исключения
    if (typeof synonyms['как'] === 'object') {
        delete synonyms['как']['сиречь'];
    }

    // 3. Чистим пустые синонимы
    console.log('\nClear empty synonyms');
    Object.keys(synonyms).forEach(wordMain => {
        if (synonyms[wordMain].length === 0) {
            delete synonyms[wordMain];
        }
    });

    // 4. Добавляем слова-синонимы, которые будут без синонимов сами
    console.log('\nAdd words without synonyms for saving');
    Object.keys(synonyms).forEach(wordMain => {
        synonyms[wordMain].forEach(synonym => {
            if (typeof synonyms[synonym] === 'undefined') {
                synonyms[synonym] = [];
            }
        });
    });

    console.log(`\nSynonyms found = ${Object.keys(synonyms).length}\nSynonyms filtered = ${synonymFiltered}`);
}


db.sequelize.sync().then(() => {

    let runTime = Date.now();

    // Получаем стемы частотности
    freqStems = getFrequencyStems();

    // Читаем и обрабатываем файл
    console.log('\n=== Start file processing ===');
    let line;
    liner = new lineByLine('./storage/base/base.txt');

    while (line = liner.next()) {
        linesTotal++;
        if (linesTotal % 500000 === 0) {
            console.log(`Processed lines ${linesTotal/1000}K`);
        }

        getPartsByLine(line);
    }

    console.log(`\n=== End file processing ===`);
    console.log(`\nTotal lines ${linesTotal}\nTotal words ${linesWords}\nStems filtered ${linesFiltered}\nFrequency filtered ${frequencyFiltered}`);
    console.log(`Filtered by props ${filteredByProps}\nFiltered by stems ${filteredByStems}`);
    console.log(`\nStems found = ${Object.keys(stems).length}\nVariants found = ${linesWords - linesFiltered - frequencyFiltered - filteredByProps - filteredByStems}`);


    console.log('\n=== Build helper arrays ===');
    let variantsArr = buildVariantsArr();

    // Читаем базу синонимов
    console.log('\n=== Loading synonyms list ===');
    getSynonyms(variantsArr);
    variantsArr = null;

    console.log('\n=== Save to DB ===');
    db.Synonym.saveList(synonyms).then(() => {
        synonyms = null;

        console.log(`\n=== Create index ===`);
        return db.sequelize.query('CREATE INDEX wordIdx ON Synonym (word)');

    }).then(() => {

        console.log(`\n=== Import done! ===\nTotal time = ${Math.round((Date.now() - runTime) / 1000 / 60)} min`);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
});
