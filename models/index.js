'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var dbs       = {};

/**
 * Возвращает подключение в БД domain
 * @param domain string
 * @param type string
 * @returns {Object}
 */
module.exports = (domain, type) => {
  domain = null;
  if (domain !== null && typeof dbs[domain] !== 'undefined') {
    return dbs[domain];
  }

  let logging = false,
      sequelize,
      db = {},
      storage = `./storage/base-${type}.sqlite`,
      config = {
        dialect: 'sqlite',
        storage: storage,
        define: {
          timestamps: false,
          freezeTableName: true
        }
      };

  try {
    logging = (require('config').get('db.logging') === true);
  } catch (err) { }

  if (env == 'production' || !logging) {
    config.logging = false;
  }

  sequelize = new Sequelize(config);

  fs.readdirSync(__dirname)
    .filter(file => {
      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    }).forEach(file => {
      let model = sequelize['import'](path.join(__dirname, file));
      db[model.name] = model;
    });

  Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });

  db.sequelize = sequelize;
  db.Sequelize = Sequelize;

  if (domain !== null) {
      dbs[domain] = db;
  }

  return db;
};
