'use strict';

module.exports = function(sequelize, DataTypes) {
  const Synonym = sequelize.define('Synonym', {
    id: {
      type:          DataTypes.INTEGER,
      allowNull:     false,
      primaryKey:    true,
      autoIncrement: true
    },
    word: {
      type:          DataTypes.STRING,
      allowNull:     false
    },
    synonyms: {
      type:          DataTypes.STRING
    }
  }, {
    createdAt: false,
    updatedAt: false,
    freezeTableName: true,
    classMethods: {

      saveList: variants => {
        return new Promise((resolve, reject) => {

          let iteration = 0;
          let bulkSize = 5000;
          let variantKeys = Object.keys(variants);
          let total = variantKeys.length;

          /**
           * Сохраняем следующую пачку синонимов
           */
          function saveNextBunch() {
            if (variantKeys.length <= 0) {
              console.log(`Synonyms saved`);

              return Synonym.updateList(variants)
                .then(resolve)
                .catch(reject);
            }


            iteration++;
            if (iteration * bulkSize % 50000 === 0) {
              console.log(`Saved synonyms ${Math.floor(100 * iteration * bulkSize / total)}%`);
            }

            let saveRels = variantKeys.splice(0, bulkSize).map(word => ({
              word: word
            }));

            Synonym.bulkCreate(saveRels).then(() => {
              saveRels = null;

              return process.nextTick(function () {
                saveNextBunch();
              });
            }).catch(reject);
          }

          console.log(`Save ${variantKeys.length} synonyms`);
          saveNextBunch();
        });
      },

      /**
       * Обновляем синонимы: прописываем собственно синонимы
       * @param variants array
       * @returns {Promise}
       */
      updateList: variants => {
        console.log(`Update synonyms`);

        let iteration = 0;
        let bulkSize = 5000;
        let variantKeys = Object.keys(variants);
        let total = variantKeys.length;

        return new Promise((resolve, reject) => {

          let params = {
            attributes: ['id', 'word']
          };

          // получаем сохраненные варианты, чтобы прописать их id в синонимах
          Synonym.findAll(params).then(variantsRes => {

            // связи 'слово' => 'id'
            let variantRels = {};
            variantsRes.map(item => item.get()).forEach(variant => {
              variantRels[variant.word] = variant.id
            });
            variantsRes = null;


            function updateNextBunch() {

              iteration++;
              if (iteration * bulkSize % 20000 === 0) {
                console.log(`Updated synonyms ${Math.floor(100 * iteration * bulkSize / total)}%`);
              }

              sequelize.transaction(trans => {

                let promises = variantKeys.splice(0, bulkSize)
                  .map(word => ({
                    word:       word,
                    variantIds: variants[word].map(word => variantRels[word]).filter(item => item)
                  }))
                  .filter(item => item.variantIds.length > 0)
                  .map(item => {

                    Synonym.update({
                      synonyms: JSON.stringify(item.variantIds)
                    }, {
                      where: {
                        id: variantRels[item.word]
                      },
                      transaction: trans
                    })
                  });

                return Promise.all(promises);

              }).then(() => {

                if (variantKeys.length > 0) {
                  return process.nextTick(function () {
                    updateNextBunch();
                  });
                } else {
                  console.log(`Synonyms updated`);
                  return resolve();
                }
              }).catch(reject);
            }

            updateNextBunch();
          });
        });
      }
    }
  });

  return Synonym;
};
